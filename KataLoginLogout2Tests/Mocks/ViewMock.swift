//
//  ViewMock.swift
//  KataLoginLogout2Tests
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation
@testable import KataLoginLogout2

class ViewMock: View
{
    var hideLoginCalls = 0
    var showLoginCalls = 0
    var showLogoutCalls = 0
    var hideLogoutCalls = 0
    var showErrorCalls = 0
    
    func hideLogin()
    {
        hideLoginCalls += 1
    }
    
    func showLogin() {
        showLoginCalls += 1
    }
    
    func showLogout() {
        showLogoutCalls += 1
    }
    
    func hideLogout() {
        hideLogoutCalls += 1
    }
    
    func showError(message: String) {
        showErrorCalls += 1
    }
    
    
}

//
//  CredentialsValidatorMock.swift
//  KataLoginLogout2Tests
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation
@testable import KataLoginLogout2

class CredentialsValidatorMock: CredentialsValidator
{
    var validateCalls = 0
    
    override func validate(email: String?, password: String?) -> [CredentialError]
    {
        validateCalls += 1
        return super.validate(email: email, password: password)
    }
}

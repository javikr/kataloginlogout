//
//  TimeProviderMock.swift
//  KataLoginLogout2Tests
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation
@testable import KataLoginLogout2

class TimeProviderStub: TimeProvider
{
    var currentSecondReturnValue: Int = 0
    
    override func obtainCurrentSecond() -> Int
    {
        return currentSecondReturnValue
    }
}

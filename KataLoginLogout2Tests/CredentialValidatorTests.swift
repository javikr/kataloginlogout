//
//  CredentialValidatorTests.swift
//  KataLoginLogout2Tests
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import XCTest
@testable import KataLoginLogout2

class CredentialValidatorTests: XCTestCase
{
    private var credentialsValidator: CredentialsValidator!
    
    override func setUp()
    {
        super.setUp()
        
        credentialsValidator = CredentialsValidator()
    }
    
    override func tearDown()
    {
        credentialsValidator = nil
        
        super.tearDown()
    }

    func testGivenValidPasswordAndValidEmailThenValidatorMustReturnTrue()
    {
        let email = "pepe@pepe.com"
        let password = "validpassword"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.isEmpty)
    }
    
    func testGivenEmptyEmailThenValidatorMustReturnFalse()
    {
        let email = ""
        let password = "validpassword"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.email))
    }
    
    func testGivenNullEmailThenValidatorMustReturnFalse()
    {
        let email: String? = nil
        let password = "validpassword"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.email))
    }
    
    func testGivenInvalidEmailThenValidatorMustReturnFalse()
    {
        let email = "bademail"
        let password = "validpassword"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.email))
    }
    
    func testGivenValidPasswordThenValidatorMustReturnTrue()
    {
        let email = "pepe@pepe.com"
        let password = "validpassword"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.isEmpty)
    }
    
    func testGivenEmptyPasswordThenValidatorMustReturnFalse()
    {
        let email = "pepe@pepe.com"
        let password = ""
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.password))
    }
    
    func testGivenNullPasswordThenValidatorMustReturnFalse()
    {
        let email = "pepe@pepe.com"
        let password: String? = nil
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.password))
    }
    
    func testGivenInvalidPasswordThenValidatorMustReturnFalse()
    {
        let email = "pepe@pepe.com"
        let password = "pas"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.password))
    }
    
    func testGivenInValidPasswordAndInValidEmailThenValidatorMustReturnTrue()
    {
        let email = "bademail"
        let password = "pas"
        
        let errors = credentialsValidator.validate(email: email, password: password)
        
        XCTAssertTrue(errors.contains(.password))
        XCTAssertTrue(errors.contains(.email))
    }
}

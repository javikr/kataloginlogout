//
//  PresenterTests.swift
//  KataLoginLogout2Tests
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import XCTest
@testable import KataLoginLogout2

class PresenterTests: XCTestCase
{
    var presenter: Presenter!
    var validator: CredentialsValidatorMock!
    var apiClient: ApiClientMock!
    var view: ViewMock!
    var timeProvider: TimeProviderStub!
    
    override func setUp()
    {
        super.setUp()
        
        timeProvider = TimeProviderStub()
        validator = CredentialsValidatorMock()
        apiClient = ApiClientMock(timeProvider: timeProvider)
        view = ViewMock()
        
        presenter = Presenter(view: view, validator: validator, apiClient: apiClient)
    }
    
    override func tearDown()
    {
        presenter = nil
        super.tearDown()
    }
    
    func testViewLoaded()
    {
        presenter.viewLoaded()
        
        XCTAssertEqual(view.hideLogoutCalls, 1)
    }

    func testDidTapLoginButtonWithInvalidFields()
    {
        presenter.didTapLoginButton(email: "invalid", password: "aa")
        
        XCTAssertEqual(validator.validateCalls, 1)
        XCTAssertEqual(apiClient.loginCalls, 0)
        XCTAssertEqual(view.showErrorCalls, 1)
    }
    
    func testDidTapLoginButtonWithIncorrectFields()
    {
        presenter.didTapLoginButton(email: "valid@valid.com", password: "password")
        
        XCTAssertEqual(validator.validateCalls, 1)
        XCTAssertEqual(apiClient.loginCalls, 1)
    }
    
    func testDidTapLoginButtonWithValidFields()
    {
        presenter.didTapLoginButton(email: "root@idealista.com", password: "idealista")
        
        XCTAssertEqual(validator.validateCalls, 1)
        XCTAssertEqual(apiClient.loginCalls, 1)
        XCTAssertEqual(view.hideLoginCalls, 1)
        XCTAssertEqual(view.showLogoutCalls, 1)
    }
}

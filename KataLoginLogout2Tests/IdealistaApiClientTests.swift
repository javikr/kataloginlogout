//
//  IdealistaApiClientTests.swift
//  KataLoginLogout2Tests
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import XCTest
@testable import KataLoginLogout2

class IdealistaApiClientTests: XCTestCase
{
    var apiClient: IdealistaApiClient!
    var timeProviderStub: TimeProviderStub!
    
    private var oddIntValue = 3
    private var pairIntValue = 2
    
    let invalidEmail = "bademail@bad.com"
    let validEmail = "root@idealista.com"
    let invalidPassword = "badpassword"
    let validPassword = "idealista"
    
    override func setUp()
    {
        super.setUp()
        
        timeProviderStub = TimeProviderStub()
        apiClient = IdealistaApiClient(timeProvider: timeProviderStub)
    }
    
    override func tearDown()
    {
        apiClient = nil
        
        super.tearDown()
    }
    
    // MARK: - Test
    
    func testLogoutWithOddSecondShouldFail()
    {
        timeProviderStub.currentSecondReturnValue = oddIntValue
        
        XCTAssertFalse(apiClient.logout())
    }
    
    func testLogoutWithPairSecondShouldWork()
    {
        timeProviderStub.currentSecondReturnValue = pairIntValue
        
        XCTAssertTrue(apiClient.logout())
    }
    
    func testLoginWithInvalidEmailValidPassword()
    {
        XCTAssertFalse(apiClient.login(withEmail: invalidEmail, password: validPassword))
    }
    
    func testLoginWithValidEmailInvalidPassword()
    {
        XCTAssertFalse(apiClient.login(withEmail: validEmail, password: invalidPassword))
    }
    
    func testLoginWithValidEmailValidPassword()
    {
        XCTAssertTrue(apiClient.login(withEmail: validEmail, password: validPassword))
    }
    
    func testLoginWithInvalidPasswordInvalidEmail()
    {
        XCTAssertFalse(apiClient.login(withEmail: invalidEmail, password: invalidPassword))
    }
    
    func testLoginWithNilPasswordNilEmail()
    {
        XCTAssertFalse(apiClient.login(withEmail: nil, password: nil))
    }
    
}

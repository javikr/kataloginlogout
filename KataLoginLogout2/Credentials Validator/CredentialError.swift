//
//  CredentialError.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation

enum CredentialError
{
    case email
    case password
}

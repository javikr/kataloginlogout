//
//  CredentialsValidator.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation

class CredentialsValidator
{
    func validate(email: String?, password: String?) -> [CredentialError]
    {
        var errors = [CredentialError]()
        
        if !validate(email: email) {
            errors.append(.email)
        }
        
        if !validate(password: password) {
            errors.append(.password)
        }
        
        return errors
    }
    
    // MARK: - Private
    
    private func validate(email: String?) -> Bool
    {
        guard let email = email else { return false }
        
        return email.contains("@")
    }
    
    private func validate(password: String?) -> Bool
    {
        guard let password = password else { return false }
        
        return password.count > 4
    }
}

//
//  Presenter.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation

class Presenter {
    
    private weak var view: View?
    private let validator: CredentialsValidator
    private let apiClient: IdealistaApiClient
    
    init(view: View, validator: CredentialsValidator, apiClient: IdealistaApiClient) {
        self.view = view
        self.validator = validator
        self.apiClient = apiClient
    }
    
    func viewLoaded() {
        view?.hideLogout()
    }
    
    func didTapLoginButton(email: String?, password: String?) {
        guard validateFields(email: email, password: password) else { return }
        
        if apiClient.login(withEmail: email, password: password) {
            view?.hideLogin()
            view?.showLogout()
        } else {
            view?.showError(message: "Invalid credentials :(")
        }
    }
    
    func didTapLogoutButton() {
        if apiClient.logout() {
            view?.showLogin()
            view?.hideLogout()
        } else {
            view?.showError(message: "Logout error")
        }
    }
    
    // MARK : - private
    
    private func validateFields(email: String?, password: String?) -> Bool
    {
        let credentialErrors = validator.validate(email: email, password: password)
        let invalidEmail = credentialErrors.contains(.email)
        let invalidPassword = credentialErrors.contains(.password)
        
        if invalidEmail && invalidPassword {
            view?.showError(message: "Email y Password error")
            return false
        }
        
        if invalidEmail {
            view?.showError(message: "Email error")
            return false
        }
        
        if invalidPassword {
            view?.showError(message: "Password error")
            return false
        }
        
        return true
    }
    
}

//
//  ViewController.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    var presenter: Presenter?
    private let credentialsValidator = CredentialsValidator()
    private let apiClient = IdealistaApiClient(timeProvider: TimeProvider())

    @IBOutlet weak var emailLabel: UILabel! {
        didSet {
            emailLabel.text = "Email"
        }
    }
    @IBOutlet weak var passwordLabel: UILabel! {
        didSet {
            passwordLabel.text = "Password"
        }
    }
    @IBOutlet weak var loginButton: UIButton! {
        didSet {
            loginButton.setTitle("Login", for: .normal)
        }
    }
    @IBOutlet weak var logoutButton: UIButton! {
        didSet {
            logoutButton.setTitle("Logout", for: .normal)
        }
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        presenter = Presenter(view: self, validator: credentialsValidator, apiClient: apiClient)
        presenter?.viewLoaded()
    }

    // MARK: - Button
    
    @IBAction func didTapButton(_ sender: Any)
    {
        presenter?.didTapLoginButton(email: emailTextField.text, password: passwordTextField.text)
    }
    
    @IBAction func didTapLogout(_ sender: Any)
    {
        presenter?.didTapLogoutButton()
    }
}

extension ViewController : View {
    
    func hideLogin() {
        loginButton.isHidden = true
        emailTextField.isHidden = true
        passwordTextField.isHidden = true
        emailLabel.isHidden = true
        passwordLabel.isHidden = true
    }
    
    func showLogin() {
        loginButton.isHidden = false
        emailTextField.isHidden = false
        passwordTextField.isHidden = false
        emailLabel.isHidden = false
        passwordLabel.isHidden = false
    }
    
    func showLogout() {
        logoutButton.isHidden = false
    }
    
    func hideLogout() {
        logoutButton.isHidden = true
    }
    
    func showError(message: String) {
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        let alertView = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertView.addAction(okAction)
        present(alertView, animated: true, completion: nil)
    }
    
}


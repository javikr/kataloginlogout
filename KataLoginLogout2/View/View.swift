//
//  View.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation

protocol View : class {
    func hideLogin()
    func showLogin()
    func showLogout()
    func hideLogout()
    func showError(message: String)
}

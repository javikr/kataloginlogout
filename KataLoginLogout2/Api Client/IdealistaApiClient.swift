//
//  IdealistaApiClient.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation

class IdealistaApiClient
{
    let timeProvider: TimeProvider
    
    private let expectedEmail = "root@idealista.com"
    private let expectedPassword = "idealista"
    
    init(timeProvider: TimeProvider)
    {
        self.timeProvider = timeProvider
    }
    
    func login(withEmail email: String?, password: String?) -> Bool
    {
        return doLogin(email: email, password: password)
    }
    
    func logout() -> Bool
    {
        return doLogout()
    }
    
    // MARK: - Private
    
    private func doLogout() -> Bool
    {
        return timeProvider.obtainCurrentSecond() % 2 == 0
    }
    
    private func doLogin(email: String?, password: String?) -> Bool
    {
        guard let email = email, let password = password else { return false }
        
        return email == expectedEmail && password == expectedPassword
    }
}

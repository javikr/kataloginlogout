//
//  TimeProvider.swift
//  KataLoginLogout2
//
//  Created by Javier Aznar on 18/9/17.
//  Copyright © 2017 VictorJavi. All rights reserved.
//

import Foundation

class TimeProvider
{
    func obtainCurrentSecond() -> Int
    {
        let date = Date()
        let calendar = Calendar.current
        
        return calendar.component(.second, from: date)
    }
}
